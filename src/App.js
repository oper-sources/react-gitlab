import React, { useState } from 'react';
import { Steps, Button, message } from 'antd';
import './App.css';

function App() {

  const { Step } = Steps;

  const steps = [
    {
      title: 'First',
      content: 'First-content',
    },
    {
      title: 'Second',
      content: 'Second-content',
    },
    {
      title: 'Last',
      content: 'Last-content',
    },
  ];

  const [current, setCount] = useState(0);

  return (
    <div className="App">
      <h1>Hi, I'm a Test</h1>
      <div>
        <Steps direction="vertical" current={current}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">{steps[current].content}</div>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => setCount(current + 1)}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" onClick={() => message.success('Processing complete!')}>
              Done
            </Button>
          )}
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => setCount(current - 1)}>
              Previous
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
